import json
import requests


from django.shortcuts import redirect
from django.utils.translation import gettext_lazy as _
from django.views.generic import TemplateView

from pretalx.cfp.flow import GenericFlowStep, FormFlowStep
from pretalx.event.models import Event
from pretalx.schedule.models import Room

from pretalx_roompref.forms import RoomPrefForm

class RoomPrefStep(GenericFlowStep, FormFlowStep):
    icon = "info-circle"
    identifier = "roompref_select"
    priority = 50
    form_class = RoomPrefForm
    template_name = "pretalx_roompref/roompref_step.html"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
    
    @property
    def title(self):
        return _("Room preferences")

    @property
    def label(self):
        return _("Room preferences")

    @property
    def text(self):
        return _("On this page you can see the different spaces that are available. "
        "Select which you think are the most adapted to your proposal.")

    def done(self, request, draft=False):
        form = self.get_form(from_storage=True)
        form.speaker = request.user
        form.submission = request.submission
        form.is_valid()
        form.save()
        
    def get_context_data(self):
        from pretalx.cfp.flow import i18n_string
        result = super().get_context_data()
        question_pk = self.event.settings.get(f'roompref_question_rooms')
        choices_map = json.loads(self.event.settings.get(\
                        f'roompref_question_rooms_choices'))
        choices_map = {int(k): v for k, v in choices_map.items()}
        question_id = f'question_{question_pk}'
        form = self.get_form()
        field = form.fields[question_id]
        result['rooms'] = rooms = list()
        
        for room in Room.objects.order_by("position"):
            choice = field.queryset.get(pk=choices_map[room.id])
            rooms.append((room, choice))

        result['rooms_question_id'] = question_id
        return result
        

class RoomPublicList(TemplateView):
    template_name = "pretalx_roompref/roompref_public_list.html"
    
    
    @property
    def title(self):
        return _("Available room list")
        
    @property
    def label(self):
        return _("Available room list")
        
    @property
    def text(self):
        return _("The followings places are availables for your propositions at")
    
    def get_queryset(self, *args, **kwargs):
        return Room.objects.order_by("position")

    def get_context_data(self, **kwargs):
        ctx = super(RoomPublicList, self).get_context_data(**kwargs)
        ctx['title'] = self.title
        ctx['label'] = self.label
        ctx['text'] = self.text
        return ctx

