from django import forms
from django.db.models import Q

from pretalx.cfp.forms.cfp import CfPFormMixin
from pretalx.common.mixins.forms import QuestionFieldsMixin
from pretalx.submission.models import Question, QuestionVariant, QuestionTarget


class RoomPrefForm(CfPFormMixin, QuestionFieldsMixin, forms.Form):
    def __init__(self, *args, **kwargs):
        self.event = kwargs.pop("event", None)
        readonly = kwargs.pop("readonly", False)

        super().__init__(*args, **kwargs)
        question = Question.all_objects.get(pk=self.event.settings.get("roompref_question_rooms"))
        initial = question.default_answer
        initial_object = None
        field = self.get_field(
            question=question, readonly=readonly,
            initial=question.default_answer, initial_object=None
        )
        field.question = question
        field.answer = initial_object
        self.fields[f"question_{question.pk}"] = field

    def save(self):
        for k, v in self.cleaned_data.items():
            self.save_questions(k, v)
