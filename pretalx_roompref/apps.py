from django.apps import AppConfig
from django.utils.translation import gettext_lazy as _

from django.dispatch import receiver

ROOM_PREFERENCE_QUESTION = {
    'variant': 'multiple_choice',
    'target': 'submission',
    'question_required': False,
    'question': _("What room(s) would you prefer to use ?"),
    'help_text': _("This can help the organiser for make a better schedule."),
    'contains_personal_data': False,
    'is_public': False,
    'is_visible_to_reviewers': True
}

## Remove or disable question when uninstall ?
## remove for remove question, disable for anything else.
ON_UNINSTALL = "remove"

class PluginApp(AppConfig):
    name = "pretalx_roompref"
    verbose_name = "Room self-selection"
    
    AO = None
    Q = None

    class PretalxPluginMeta:
        name = _("Room self-selection")
        author = "BIB Hackerspace"
        description = _("A plug-in to allow participants to select their preferred room when submitting proposals.")
        visible = True
        version = "0.3.1"

    def ready(self):
        from . import signals  # NOQA
        from pretalx.schedule.signals import rooms_update
        
        @receiver(rooms_update)
        def reinstall(sender, **kwargs):
            self.installed(sender)
        self._reinstaller = reinstall
        

    def get_room_choices(self):
        
        from pretalx.schedule.models import Room
        choices = Room.objects

        return choices

    def build_questions(self):
        return {
            'rooms': (ROOM_PREFERENCE_QUESTION, self.get_room_choices())
        }
    
    def del_answerOpt(self, answerOpt, choices_map=None):
        if choices_map :
            key = next((key for key, value in choices_map.items() \
                        if value == answerOpt.id), None)
            choices_map.pop(key)
        answerOpt.delete()
        
        return choices_map
        
    def refresh_answerOpt(self, answerOpt, text):
        answerOpt.answer = text
        answerOpt.save()
    
    def add_answerOpt(self, question, text, map_id=None, choices_map=None):
        answerOpt = self.AO.objects.get_or_create(question=question, answer=text)
        if choices_map is not None:
            choices_map[map_id] = answerOpt[0].id
        
        return choices_map

    def create_question(self, event, name, data):
        from pretalx.cfp.flow import i18n_string

        question = self.Q(
            event=event,
            question=i18n_string(data['question'], event.locales)
        )
        question.variant = data['variant']
        question.target = data['target']
        question.question_required = data['question_required']
        question.question = i18n_string(data['question'], event.locales)
        question.help_text = i18n_string(data['help_text'], event.locales)
        question.contains_personal_data = data['contains_personal_data']
        question.is_public = data['is_public']
        question.is_visible_to_reviewers = data['is_visible_to_reviewers']
        question.active = True
        question.internal = True
        question.save()
        event.settings.set(
            f"roompref_question_{name}", question.pk
        )
        
        return question
        
    def remove_question(self, event, name, question):

        if ON_UNINSTALL == "remove" :

            for answer in question.answers.all():
                answer.remove()
            question.options.all().delete()
            question.logged_actions().delete()
            question.delete()
            event.settings.delete(f"roompref_question_{name}")
            event.settings.delete(f"roompref_question_{name}_choices")
        else:
            question.active = False
            question.save()
        
    def populate_answers(self, event, name, question, options):
        import json
        choices_map = dict()
        
        if name == "rooms" :

            rooms = options
            
            if event.settings.get(f"roompref_question_{name}_choices") :
                choices_map = json.loads(\
                            event.settings.get\
                            (f"roompref_question_{name}_choices"))
                choices_map = {int(k): v for k, v in choices_map.items()}

            rooms_ids = rooms.values_list('id', flat=True)
            
            answ_to_remove = [choices_map[x] for x in \
                            choices_map.keys() - rooms_ids]
            answ_to_refresh = list(zip(*sorted([(choices_map[x], x) for x in \
                            set(choices_map.keys()).intersection(rooms_ids)])))
            if not answ_to_refresh:
                answ_to_refresh = ((),())
            add_answ_for_rooms = set(rooms_ids) - set(choices_map.keys())

            ## Remove non relevent answer option
            for answOpt in self.AO.objects.filter\
                        (question=question, id__in=answ_to_remove):
                choices_map = self.del_answerOpt(answOpt, choices_map)
            
            ## Refresh existing answers option
            for i, answOpt in enumerate(self.AO.objects.filter\
                        (question=question, id__in=answ_to_refresh[0])):
                room = rooms.get(id=answ_to_refresh[1][i])
                self.refresh_answerOpt(answOpt, room.name)
                
            ## Add new answers option
            for room in rooms.filter(id__in=add_answ_for_rooms):
                choices_map = self.add_answerOpt(\
                            question, room.name, room.id, choices_map)
            
            
            event.settings.set(
                f"roompref_question_{name}_choices", \
                json.dumps(choices_map)
            )
                
        

    def installed(self, event):
        pt_cfp_m = __import__("pretalx.submission.models")
        self.AO = pt_cfp_m.submission.models.question.AnswerOption
        self.Q = pt_cfp_m.submission.models.question.Question
        
        
        
        for name, (q_data, options) in self.build_questions().items():
            question = None
            pk_question = event.settings.get(f"roompref_question_{name}")
            if pk_question:
                question = self.Q.all_objects.filter(pk=pk_question, event=event).first()
            
            if not question:
                question = self.create_question(event, name, q_data)
            
            if options is not None:
                self.populate_answers(event, name, question, options)
                

    def uninstalled(self, event):
        pt_cfp_m = __import__("pretalx.submission.models")
        self.Q = pt_cfp_m.submission.models.question.Question

        for name in self.build_questions():
            question = None
            pk = event.settings.get(f"roompref_question_{name}")
            if pk:
                question = self.Q.all_objects.filter(pk=pk, event=event).first()
            if question:
                self.remove_question(event, name, question)
