
from django.urls import re_path

from pretalx.event.models.event import SLUG_CHARS

from . import views

urlpatterns = [
    re_path(
            f"^(?P<event>[{SLUG_CHARS}]+)/p/availablerooms/$",
            views.RoomPublicList.as_view(), 
            name='frontend'),
        ]
