from pretalx.cfp.signals import cfp_steps
from django.dispatch import receiver

@receiver(cfp_steps)
def roompref_cfp_steps(sender, **kwargs):
    from .views import RoomPrefStep

    return [RoomPrefStep]
