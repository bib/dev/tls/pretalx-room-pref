all: localecompile

localecompile:
	django-admin compilemessages

localegen:
	django-admin makemessages -l de_DE -i build -i dist -i "*egg*"
	django-admin makemessages -l fr_FR -i build -i dist -i "*egg*"

